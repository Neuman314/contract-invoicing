# Contract Invoice Application

This is a simple demo application of a primitive contract invoicing system.

This contains 3 entities

- User
- Contract
- Invoice

## Building and running the application.

This demo uses H2, which is an in memory database making setup and running a breeze.

simply run 

```bash
./gradlew build 
```

to build and run integration tests (also using h2)

To run the program, run 

```bash
java -jar build/libs/contract-0.0.1-SNAPSHOT.jar
```

To run integration tests run 

```bash
./gradlew test
```

### User Controller

The user controller, for demo purposes, allows for the current logged in user 
to be switched. By default, you are logged in as a contractor user.

#### **GET** _/whoami_ returns the current loggedin user

#### **POST** _/loginas_ changes the current loggedin user the id provided 

Example 

```bash
curl http://localhost:8080/loginas -X POST -d '{"id":"2"}' -H 'Content-Type: application/json' | python -m json.tool
```

Response

```json
{
    "createdOn": null,
    "email": "client@test.com",
    "firstName": "Client",
    "id": 2,
    "lastName": "Demo",
    "updatedOn": null
}
```

Will log you in as a client user
_/whoami_
```bash
curl http://localhost:8080/whoami | python -m json.tool
```
Response
```json
{
    "email": "client@test.com",
    "firstName": "Client",
    "id": 2,
    "lastName": "Demo",
    "updatedOn": null
}
```

The application by default is populated with 3 test users. 

- Id 1 Default, contractor.

- Id 2 Customer, has 1 contract with contractor user Id 1

- Id 3 Customer, no contracts.

use the /loginas endpoint to switch between these 3 users to change context.

### Contracts

#### **GET** _/contract_ Returns a list of contracts where the logged-in user is either a customer or a contractor

Example

```bash
curl http://localhost:8080/contract | python -m json.tool
```
Response
```json
[
    {
        "contractorUserId": 1,
        "createdOn": "2021-12-06T15:11:50.744+00:00",
        "customerUserId": 2,
        "deadline": "2022-02-01T05:00:00.000+00:00",
        "description": "New Roof Installation",
        "id": 100,
        "maxValue": 1000.0,
        "role": "VENDOR",
        "status": "APPROVED",
        "updatedOn": null
    }
]
```
 Example (Filter by role ) (CLIENT,VENDOR) In test data, role = CLIENT returns []
```bash
curl http://localhost:8080/contract\?role\=VENDOR | python -m json.tool
```
Response
```json
[
    {
        "contractorUserId": 1,
        "createdOn": "2021-12-06T15:11:50.744+00:00",
        "customerUserId": 2,
        "deadline": "2022-02-01T05:00:00.000+00:00",
        "description": "New Roof Installation",
        "id": 100,
        "maxValue": 1000.0,
        "role": "VENDOR",
        "status": "APPROVED",
        "updatedOn": null
    }
]
```

#### **POST** _/contract_ Creates a new contract. Only Contractor users can post new contracts

```bash
curl http://localhost:8080/contract -H 'Content-Type: application/json' -d '{
        "description": "Program contracting app",
        "customerId": 2,
        "maxValue": 10.00,
        "deadline": "2022-01-01T00:00:00.000"
      }' | python -m json.tool
```

Response

```json
{
    "contractorUserId": 1,
    "createdOn": "2021-12-06T16:13:45.830+00:00",
    "customerUserId": 2,
    "deadline": "2022-01-01T00:00:00.000+00:00",
    "description": "Program contracting app",
    "id": 4,
    "maxValue": 10.0,
    "role": "VENDOR",
    "status": "SUBMITTED",
    "updatedOn": "2021-12-06T16:13:45.830+00:00"
}
```

If you are logged in as a client user, you will get the following error when you try to create a new contract

```json
{
    "code": "INVALID_ROLE",
    "message": "User must be a contractor in order to perform this action"
}
```

### Invoices

Contracts and invoices represent a **One to Many** relationship where each contract can have many invoices. Each invoice can only have 1 Contract.

#### **GET** _/contract/{contractId}/invoice returns a list of invoices per the given contract

```bash
curl http://localhost:8080/contract/100/invoice | python -m json.tool
```
Response
```json
[
    {
        "contractId": 100,
        "cost": 25.0,
        "createdOn": "2021-12-06T15:11:50.745+00:00",
        "description": "Inspection Fee (Void on sign)",
        "id": 100,
        "status": "VOID",
        "updatedOn": null
    },
    {
        "contractId": 100,
        "cost": 450.0,
        "createdOn": "2021-12-06T15:11:50.745+00:00",
        "description": "Cost of Shingles",
        "id": 200,
        "status": "SUBMITTED",
        "updatedOn": null
    }
]
```

#### **POST** _/contract/{contractId}/invoice_

Example

```bash
curl http://localhost:8080/contract/100/invoice -H 'Content-Type: application/json' 
-d '{
      "description": "Install Drip Edge",
      "cost": 300.50
    }' | python -m json.tool
```
Response
```json
{
    "contractId": 100,
    "cost": 300.5,
    "createdOn": "2021-12-06T15:28:54.697+00:00",
    "description": "Install Drip Edge",
    "id": 3,
    "status": "SUBMITTED",
    "updatedOn": "2021-12-06T15:28:54.697+00:00"
}
```

If you attempt to post an invoice where the cost exceeds the **maxValue** field on the contract, 
including other existing non-VOID invoices on that contract, you will get the following error

```json
{
    "code": "CONTRACT_MAX",
    "message": "Contract max value of 1000.00 has been exceeded."
}
```

#### PUT _/contract/{contractId}/invoice/{invoiceId}_

Updates the status and description of the invoice.

Example

```bash
curl http://localhost:8080/contract/100/invoice/200 -X PUT -H 'Content-Type: application/json' 
-d '{
      "description": "Free Shingles",
      "status": "VOID"
    }' | python -m json.tool
```

```json
{
    "contractId": 100,
    "cost": 450.0,
    "createdOn": "2021-12-06T15:11:50.745+00:00",
    "description": "Free Shingles",
    "id": 200,
    "status": "VOID",
    "updatedOn": "2021-12-06T16:08:25.056+00:00"
}
```