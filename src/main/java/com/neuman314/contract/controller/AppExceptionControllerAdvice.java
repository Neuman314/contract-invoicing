package com.neuman314.contract.controller;

import com.neuman314.contract.dto.ApplicationErrorDTO;
import com.neuman314.contract.exception.AppException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AppExceptionControllerAdvice {

    @ExceptionHandler(AppException.class)
    public ResponseEntity<ApplicationErrorDTO> handleAppException(AppException exception) {
        return new ResponseEntity<ApplicationErrorDTO>(
                new ApplicationErrorDTO(exception.getCode(), exception.getMessage()), exception.getHttpStatus());
    }
}
