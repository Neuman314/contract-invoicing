package com.neuman314.contract.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {

    @GetMapping(value = "/")
    public ResponseEntity<String> get() {
        return ResponseEntity.ok("Hello World!");
    }
}
