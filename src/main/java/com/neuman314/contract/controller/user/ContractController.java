package com.neuman314.contract.controller.user;

import com.neuman314.contract.dto.AddContractDTO;
import com.neuman314.contract.dto.ContractDTO;
import com.neuman314.contract.service.ContractService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ContractController {

    private final ContractService contractService;


    public ContractController(ContractService contractService) {
        this.contractService = contractService;
    }

    @GetMapping(value = "/contract")
    public ResponseEntity<List<ContractDTO>> getCustomerContracts() {
        return ResponseEntity.ok(contractService.getAll());
    }

    @GetMapping(value = "/contract", params = "role")
    public ResponseEntity<List<ContractDTO>> getCustomerContractByRole(@RequestParam ContractDTO.UserRole role) {
        return ResponseEntity.ok(contractService.getFilterByUserRole(role));
    }

    @PostMapping(value = "/contract")
    public ResponseEntity<ContractDTO> add(@RequestBody AddContractDTO add) {
        return ResponseEntity.ok(contractService.add(add));
    }
}
