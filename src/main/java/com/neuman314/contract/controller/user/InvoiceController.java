package com.neuman314.contract.controller.user;

import com.neuman314.contract.dto.AddInvoiceDTO;
import com.neuman314.contract.dto.InvoiceDTO;
import com.neuman314.contract.dto.UpdateInvoiceDTO;
import com.neuman314.contract.service.InvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InvoiceController {

    private final InvoiceService invoiceService;


    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/contract/{contractId}/invoice")
    public ResponseEntity<List<InvoiceDTO>> get(@PathVariable Long contractId) {
        return ResponseEntity.ok(invoiceService.getByContractId(contractId));
    }

    @PostMapping("/contract/{contractId}/invoice")
    public ResponseEntity<InvoiceDTO> add(@PathVariable Long contractId, @RequestBody AddInvoiceDTO add) {
        return ResponseEntity.ok(invoiceService.add(contractId, add));
    }

    @GetMapping("/contract/{contractId}/invoice/{invoiceId}")
    public ResponseEntity<InvoiceDTO> getOn(@PathVariable Long contractId, @PathVariable Long invoiceId) {
        return ResponseEntity.ok(invoiceService.findByInvoiceId(contractId, invoiceId));
    }

    @PutMapping("/contract/{contractId}/invoice/{invoiceId}")
    public ResponseEntity<InvoiceDTO> update(@PathVariable Long contractId, @PathVariable Long invoiceId, @RequestBody UpdateInvoiceDTO update) {
        return ResponseEntity.ok(invoiceService.update(contractId, invoiceId, update));
    }
}
