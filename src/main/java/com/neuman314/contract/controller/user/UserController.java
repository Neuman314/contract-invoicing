package com.neuman314.contract.controller.user;

import com.neuman314.contract.dto.UserDTO;
import com.neuman314.contract.orm.user.User;
import com.neuman314.contract.request.DemoUserRequestProvider;
import com.neuman314.contract.service.UserService;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Profile("demo")
public class UserController {

    private final UserService userService;

    private final DemoUserRequestProvider userRequestProvider;

    public UserController(UserService userService, DemoUserRequestProvider userRequestProvider) {
        this.userService = userService;
        this.userRequestProvider = userRequestProvider;
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<UserDTO> get(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getOne(id));
    }

    @GetMapping("/user")
    public ResponseEntity<List<UserDTO>> get() {
        return ResponseEntity.ok(userService.getAll());
    }

    @GetMapping("/whoami")
    public ResponseEntity<UserDTO> whoami() {
        return ResponseEntity.ok(User.asDTO(userRequestProvider.get()));
    }

    @PostMapping("/loginas")
    public ResponseEntity<UserDTO> loginas(@RequestBody UserDTO user) {
        return ResponseEntity.ok(userRequestProvider.setLoggedInId(user));
    }
}
