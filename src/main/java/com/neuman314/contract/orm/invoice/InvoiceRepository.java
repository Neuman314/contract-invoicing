package com.neuman314.contract.orm.invoice;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

    List<Invoice> findAllByContractId(Long contractId);

    Optional<Invoice> findByContractIdAndId(Long contractId, Long invoiceId);
}
