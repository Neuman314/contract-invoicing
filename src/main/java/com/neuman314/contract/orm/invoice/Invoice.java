package com.neuman314.contract.orm.invoice;

import com.neuman314.contract.dto.AddInvoiceDTO;
import com.neuman314.contract.dto.InvoiceDTO;
import com.neuman314.contract.dto.UpdateInvoiceDTO;
import com.neuman314.contract.orm.contract.Contract;
import com.neuman314.contract.orm.contract.ContractStatus;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "invoice")
public class Invoice {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Long contractId;

    @Column
    private BigDecimal cost;

    @Column
    @CreationTimestamp
    private Timestamp createdOn;

    @Column
    @UpdateTimestamp
    private Timestamp updatedOn;

    @Column
    private String description;

    @Column
    @Enumerated(EnumType.STRING)
    private ContractStatus status;

    @ManyToOne
    @JoinColumn(name = "contractId", referencedColumnName = "id", insertable = false, updatable = false)
    private Contract contract;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal value) {
        this.cost = value;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ContractStatus getStatus() {
        return status;
    }

    public void setStatus(ContractStatus status) {
        this.status = status;
    }

    public Contract getContract() {
        return contract;
    }

    public static InvoiceDTO asDTO(Invoice invoice) {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        invoiceDTO.id = invoice.getId();
        invoiceDTO.contractId = invoice.getContractId();
        invoiceDTO.description = invoice.getDescription();
        invoiceDTO.cost = invoice.getCost();
        invoiceDTO.createdOn = invoice.getCreatedOn();
        invoiceDTO.updatedOn = invoice.getUpdatedOn();
        invoiceDTO.status = invoice.getStatus().name();
        return invoiceDTO;
    }

    public static Invoice newInvoice(Long contractId, AddInvoiceDTO add) {
        Invoice invoice = new Invoice();
        invoice.description = add.description;
        invoice.cost = add.cost;
        invoice.status = ContractStatus.SUBMITTED;
        invoice.contractId = contractId;
        return invoice;
    }

    public static Invoice applyUpdate(Invoice invoice, UpdateInvoiceDTO update) {
        invoice.setDescription(update.description);
        invoice.setStatus(ContractStatus.validateStatus(update.status));
        return invoice;
    }
}
