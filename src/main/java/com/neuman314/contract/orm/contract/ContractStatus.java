package com.neuman314.contract.orm.contract;

import com.neuman314.contract.exception.AppException;

public enum ContractStatus {
    SUBMITTED,
    IN_PROGRESS,
    APPROVED,
    VOID,
    ;

    public static ContractStatus validateStatus(String name) {
        try {
            return ContractStatus.valueOf(name);
        } catch (Exception e) {
            throw AppException.new400Exception("INVALID_ARGUMENT", String.format("Status: %s is not valid", name));
        }
    }
}
