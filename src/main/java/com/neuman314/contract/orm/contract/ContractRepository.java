package com.neuman314.contract.orm.contract;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContractRepository extends CrudRepository<Contract, Long> {

    List<Contract> findAllByContractorUserId(Long contractorUserId);

    List<Contract> findAllByCustomerUserId(Long customerUserId);

    List<Contract> findAllByCustomerUserIdOrContractorUserId(Long customerUserId, Long contractorUserId);

}
