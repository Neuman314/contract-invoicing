package com.neuman314.contract.orm.contract;

import com.neuman314.contract.dto.AddContractDTO;
import com.neuman314.contract.dto.ContractDTO;
import com.neuman314.contract.orm.invoice.Invoice;
import com.neuman314.contract.orm.user.User;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "contract")
public class Contract {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Long contractorUserId;

    @ManyToOne
    @JoinColumn(name = "contractorUserId", referencedColumnName = "id", insertable = false, updatable = false)
    private User contractorUser;

    @Column
    private Long customerUserId;

    @ManyToOne
    @JoinColumn(name = "customerUserId", referencedColumnName = "id", insertable = false, updatable = false)
    private User customerUser;

    @Column
    private String description;

    @Column
    private BigDecimal maxValue;

    @CreationTimestamp
    @Column
    private Timestamp createdOn;

    @UpdateTimestamp
    @Column
    private Timestamp updatedOn;

    @Column
    private Timestamp deadline;

    @Enumerated(EnumType.STRING)
    @Column
    private ContractStatus status;

    @OneToMany(mappedBy = "contractId")
    private List<Invoice> invoices;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContractorUserId() {
        return contractorUserId;
    }

    public void setContractorUserId(Long contractorUserId) {
        this.contractorUserId = contractorUserId;
    }

    public User getContractorUser() {
        return contractorUser;
    }

    public void setContractorUser(User contractorUser) {
        this.contractorUser = contractorUser;
    }

    public Long getCustomerUserId() {
        return customerUserId;
    }

    public void setCustomerUserId(Long customerUserId) {
        this.customerUserId = customerUserId;
    }

    public User getCustomerUser() {
        return customerUser;
    }

    public void setCustomerUser(User customerUser) {
        this.customerUser = customerUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(BigDecimal maxValue) {
        this.maxValue = maxValue;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Timestamp getDeadline() {
        return deadline;
    }

    public void setDeadline(Timestamp deadline) {
        this.deadline = deadline;
    }

    public ContractStatus getStatus() {
        return status;
    }

    public void setStatus(ContractStatus status) {
        this.status = status;
    }

    public static ContractDTO asDTO(Contract contract, ContractDTO.UserRole role) {
        ContractDTO contractDTO = new ContractDTO();
        contractDTO.id = contract.id;
        contractDTO.contractorUserId = contract.contractorUserId;
        contractDTO.customerUserId = contract.customerUserId;
        contractDTO.description = contract.description;
        contractDTO.maxValue = contract.maxValue;
        contractDTO.createdOn = contract.createdOn;
        contractDTO.updatedOn = contract.updatedOn;
        contractDTO.deadline = contract.deadline;
        contractDTO.role = role.name();
        contractDTO.status = contract.status.name();
        return contractDTO;
    }

    public static Contract newContract(User contractorUser, User customerUser, AddContractDTO add) {
        Contract newContract = new Contract();
        newContract.setContractorUserId(contractorUser.getId());
        newContract.setCustomerUserId(customerUser.getId());
        newContract.setDescription(add.description);
        newContract.setDeadline(add.deadline);
        newContract.setMaxValue(add.maxValue);
        newContract.setStatus(ContractStatus.SUBMITTED);
        return newContract;
    }
}
