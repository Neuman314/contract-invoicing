package com.neuman314.contract.exception;

import org.springframework.http.HttpStatus;

public class NotFoundAppException extends AppException {
    public NotFoundAppException(String message) {
        super("NOT_FOUND", message, HttpStatus.NOT_FOUND);
    }
}
