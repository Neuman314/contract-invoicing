package com.neuman314.contract.exception;

import org.springframework.http.HttpStatus;

public class AppException extends RuntimeException {

    private final String code;

    private final HttpStatus httpStatus;

    public AppException(String code, String message, HttpStatus httpStatus) {
        super(message);
        this.code = code;
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public static AppException new400Exception(String code, String message) {
        return new AppException(code, message, HttpStatus.BAD_REQUEST);
    }
}
