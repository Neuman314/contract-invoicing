package com.neuman314.contract.dto;

public class ApplicationErrorDTO {

    public ApplicationErrorDTO(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public ApplicationErrorDTO() {
    }

    public String code;

    public String message;
}
