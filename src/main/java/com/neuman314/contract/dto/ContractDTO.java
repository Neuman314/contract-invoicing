package com.neuman314.contract.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class ContractDTO {

    public Long id;

    public String role;

    public String status;

    public Long contractorUserId;

    public Long customerUserId;

    public String description;

    public BigDecimal maxValue;

    public Timestamp createdOn;

    public Timestamp updatedOn;

    public Timestamp deadline;

    public enum UserRole {
        CLIENT,
        VENDOR,
        ;
    }
}
