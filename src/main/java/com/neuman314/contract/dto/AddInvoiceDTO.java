package com.neuman314.contract.dto;

import java.math.BigDecimal;

public class AddInvoiceDTO {

    public String description;

    public BigDecimal cost;

}
