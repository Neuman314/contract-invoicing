package com.neuman314.contract.dto;

import java.sql.Timestamp;

public class UserDTO {

    public Long id;

    public String firstName;

    public String lastName;

    public String email;

    public Timestamp createdOn;

    public Timestamp updatedOn;
}
