package com.neuman314.contract.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class AddContractDTO {

    public String description;

    public Long customerId;

    public BigDecimal maxValue;

    public Timestamp deadline;
}
