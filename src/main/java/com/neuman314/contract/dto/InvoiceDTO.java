package com.neuman314.contract.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class InvoiceDTO {
    public Long id;
    public Long contractId;
    public String description;
    public BigDecimal cost;
    public Timestamp createdOn;
    public Timestamp updatedOn;
    public String status;
}
