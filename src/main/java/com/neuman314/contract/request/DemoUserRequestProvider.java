package com.neuman314.contract.request;

import com.neuman314.contract.dto.UserDTO;
import com.neuman314.contract.exception.AppException;
import com.neuman314.contract.exception.NotFoundAppException;
import com.neuman314.contract.orm.user.User;
import com.neuman314.contract.orm.user.UserRepository;
import com.neuman314.contract.orm.user.UserType;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("demo")
public class DemoUserRequestProvider implements UserRequestProvider {

    private final UserRepository userRepository;

    public DemoUserRequestProvider(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private Long loggedInId = 1L;

    @Override
    public User get() {
        return userRepository.findById(loggedInId)
                .orElseThrow(() -> new NotFoundAppException("Demo user data not provided!!"));
    }

    @Override
    public User getContractor() {
        User user = userRepository.findById(loggedInId)
                .orElseThrow(() -> new NotFoundAppException("Demo user data not provided!!"));
        if (user.getUserType() != UserType.CONTRACTOR) {
            throw AppException.new400Exception("INVALID_ROLE", "User must be a contractor in order to perform this action");
        }
        return user;
    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundAppException("User not found"));
    }

    public UserDTO setLoggedInId(UserDTO userDTO) {
        User user = userRepository.findById(userDTO.id)
                .orElseThrow(() -> new NotFoundAppException("User not found!"));
        this.setLoggedInId(user.getId());
        return User.asDTO(user);
    }

    public Long getLoggedInId() {
        return loggedInId;
    }

    public void setLoggedInId(Long loggedInId) {
        this.loggedInId = loggedInId;
    }
}
