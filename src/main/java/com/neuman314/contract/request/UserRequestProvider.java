package com.neuman314.contract.request;

import com.neuman314.contract.orm.user.User;

/**
 * Interface for returning the active logged in user.
 */
public interface UserRequestProvider {

    /**
     * returns the current active user.
     *
     * @return
     */
    User get();

    /**
     * @return Current active user if they are a contractor
     * throws if the user is not a contractor user type
     */
    User getContractor();

    /**
     * @param Id
     * @return User by id, throws AppException if the user does not exist.
     */
    User getById(Long Id);
}
