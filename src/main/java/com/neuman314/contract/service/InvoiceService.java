package com.neuman314.contract.service;

import com.neuman314.contract.dto.AddInvoiceDTO;
import com.neuman314.contract.dto.ContractDTO;
import com.neuman314.contract.dto.InvoiceDTO;
import com.neuman314.contract.dto.UpdateInvoiceDTO;
import com.neuman314.contract.exception.AppException;
import com.neuman314.contract.exception.NotFoundAppException;
import com.neuman314.contract.orm.contract.Contract;
import com.neuman314.contract.orm.contract.ContractStatus;
import com.neuman314.contract.orm.invoice.Invoice;
import com.neuman314.contract.orm.invoice.InvoiceRepository;
import com.neuman314.contract.request.UserRequestProvider;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class InvoiceService {

    private final InvoiceRepository invoiceRepository;

    private final ContractService contractService;

    private final UserRequestProvider userRequestProvider;

    public InvoiceService(InvoiceRepository invoiceRepository, ContractService contractService, UserRequestProvider userRequestProvider) {
        this.invoiceRepository = invoiceRepository;
        this.contractService = contractService;
        this.userRequestProvider = userRequestProvider;
    }

    public List<InvoiceDTO> getByContractId(Long contractId) {
        return invoiceRepository.findAllByContractId(contractService.getById(contractId).id)
                .stream()
                .map(Invoice::asDTO)
                .collect(Collectors.toList());
    }

    public InvoiceDTO findByInvoiceId(Long contractId, Long invoiceId) {
        return Invoice.asDTO(invoiceRepository.findByContractIdAndId(contractService.getById(contractId).id, invoiceId)
                .orElseThrow(() -> new NotFoundAppException(String.format("Invoice: %d was not found", invoiceId))));
    }

    public InvoiceDTO update(Long contractId, Long invoiceId, UpdateInvoiceDTO update) {
        Invoice invoice = invoiceRepository.findById(invoiceId)
                .orElseThrow(() -> invoiceNotFound(invoiceId));
        validateContract(contractId, invoice);
        return Invoice.asDTO(invoiceRepository.save(Invoice.applyUpdate(invoice, update)));
    }


    public InvoiceDTO add(Long contractId, AddInvoiceDTO add) {
        ContractDTO contractDTO = contractService.getById(contractId);
        validateInvoiceWithinContractBudget(contractDTO.maxValue,
                getInvoiceValues(invoiceRepository.findAllByContractId(contractId), add.cost));
        return Invoice.asDTO(invoiceRepository.save(Invoice.newInvoice(contractId, add)));
    }

    public List<BigDecimal> getInvoiceValues(List<Invoice> invoices, BigDecimal valueToAdd) {
        List<BigDecimal> values = invoices.stream()
                .filter((invoice) -> invoice.getStatus() != ContractStatus.VOID)
                .map(Invoice::getCost)
                .collect(Collectors.toList());
        values.add(valueToAdd);
        return values;
    }

    public void validateInvoiceWithinContractBudget(BigDecimal maxValue, List<BigDecimal> invoiceValues) {
        BigDecimal total = invoiceValues.stream().reduce(new BigDecimal(0), BigDecimal::add);
        if (total.compareTo(maxValue) > 0) {
            throw AppException.new400Exception("CONTRACT_MAX", String.format("Contract max value of %s has been exceeded.", maxValue.toString()));
        }
    }

    private Contract validateContract(Long contractId, Invoice invoice) {
        if (contractId != invoice.getContractId() || !contractService.isContractLinkedToUser(invoice.getContract(), userRequestProvider.get())) {
            throw invoiceNotFound(invoice.getId());
        }
        return invoice.getContract();
    }

    private AppException invoiceNotFound(Long invoiceId) {
        return new NotFoundAppException(String.format("Invoice: %d was not found", invoiceId));
    }
}
