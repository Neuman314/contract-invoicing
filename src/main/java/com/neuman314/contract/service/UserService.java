package com.neuman314.contract.service;

import com.neuman314.contract.dto.UserDTO;
import com.neuman314.contract.exception.NotFoundAppException;
import com.neuman314.contract.orm.user.User;
import com.neuman314.contract.orm.user.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDTO getOne(Long id) {
        return userRepository.findById(id)
                .flatMap(user -> Optional.of(User.asDTO(user)))
                .orElseThrow(() -> new NotFoundAppException(String.format("User Id: %d not found", id)));
    }

    public List<UserDTO> getAll() {
        return StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .map(User::asDTO)
                .collect(Collectors.toList());
    }
}
