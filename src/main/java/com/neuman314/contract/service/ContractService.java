package com.neuman314.contract.service;

import com.neuman314.contract.dto.AddContractDTO;
import com.neuman314.contract.dto.ContractDTO;
import com.neuman314.contract.exception.AppException;
import com.neuman314.contract.exception.NotFoundAppException;
import com.neuman314.contract.orm.contract.Contract;
import com.neuman314.contract.orm.contract.ContractRepository;
import com.neuman314.contract.orm.user.User;
import com.neuman314.contract.request.UserRequestProvider;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContractService {

    private final UserRequestProvider userRequestProvider;

    private final ContractRepository contractRepository;

    public ContractService(UserRequestProvider userRequestProvider, ContractRepository contractRepository) {
        this.userRequestProvider = userRequestProvider;
        this.contractRepository = contractRepository;
    }

    public List<ContractDTO> getAll() {
        User user = userRequestProvider.get();
        return streamToDTO(contractRepository.findAllByCustomerUserIdOrContractorUserId(user.getId(), user.getId()));
    }

    /**
     * gets the contract by id and validates that the requesting user is either a contractor
     * or a customer of that contract.
     *
     * @param contractId
     * @return
     */
    public ContractDTO getById(Long contractId) {
        User user = userRequestProvider.get();
        Contract contract = contractRepository.findById(contractId)
                .orElseThrow(() -> contractNotFound(contractId));
        if (!isContractLinkedToUser(contract, user)) {
            throw contractNotFound(contractId);
        }
        return Contract.asDTO(contract, getUsersContractRole(contract));
    }

    Boolean isContractLinkedToUser(Contract contract, User user) {
        return contract.getContractorUserId().equals(user.getId()) || contract.getCustomerUserId().equals(user.getId());
    }

    private AppException contractNotFound(Long contractId) {
        return new NotFoundAppException(String.format("Contract: %d was not found", contractId));
    }

    public ContractDTO add(AddContractDTO add) {
        return Contract.asDTO(contractRepository.save(
                Contract.newContract(userRequestProvider.getContractor(), userRequestProvider.getById(add.customerId), add)),
                ContractDTO.UserRole.VENDOR);
    }

    public List<ContractDTO> getFilterByUserRole(ContractDTO.UserRole userRole) {
        return userRole == ContractDTO.UserRole.CLIENT ? getCustomerContracts() : getContractorContracts();
    }

    private List<ContractDTO> getCustomerContracts() {
        return streamToDTO(contractRepository.findAllByCustomerUserId(userRequestProvider.get().getId()));
    }

    private List<ContractDTO> getContractorContracts() {
        return streamToDTO(contractRepository.findAllByContractorUserId(userRequestProvider.get().getId()));
    }

    private List<ContractDTO> streamToDTO(List<Contract> contracts) {
        return contracts.stream()
                .map((contract) -> Contract.asDTO(contract, getUsersContractRole(contract)))
                .collect(Collectors.toList());
    }

    private ContractDTO.UserRole getUsersContractRole(Contract contract) {
        return userRequestProvider.get().getId().equals(contract.getContractorUserId())
                ? ContractDTO.UserRole.VENDOR
                : ContractDTO.UserRole.CLIENT;
    }
}
