insert into app_user(id, first_name, last_name, email, user_type)
values (1, 'Contractor', 'Demo', 'contractor@test.com', 'CONTRACTOR');

insert into app_user(id, first_name, last_name, email, user_type)
values (2, 'Client', 'Demo', 'client@test.com', 'CLIENT');

insert into app_user(id, first_name, last_name, email, user_type)
values (3, 'Client2', 'Demo', 'client@test.com', 'CLIENT');

insert into contract(id, contractor_user_id, customer_user_id, description, max_value, created_on, updated_on, deadline,
                     status)
values (100, 1, 2, 'New Roof Installation', 1000.00, current_timestamp(), null, '2022-02-01', 'APPROVED');

insert into invoice(id, contract_id, cost, created_on, updated_on, description, status)
values (100, 100, 25.00, current_timestamp(), null, 'Inspection Fee (Void on sign)', 'VOID');

insert into invoice(id, contract_id, cost, created_on, updated_on, description, status)
values (200, 100, 450.00, current_timestamp(), null, 'Cost of Shingles', 'SUBMITTED');

