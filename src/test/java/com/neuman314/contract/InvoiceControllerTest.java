package com.neuman314.contract;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neuman314.contract.dto.AddInvoiceDTO;
import com.neuman314.contract.dto.ApplicationErrorDTO;
import com.neuman314.contract.dto.InvoiceDTO;
import com.neuman314.contract.dto.UpdateInvoiceDTO;
import com.neuman314.contract.request.DemoUserRequestProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class InvoiceControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DemoUserRequestProvider demoUserRequestProvider;

    @BeforeEach
    public void setup() throws Exception {
        demoUserRequestProvider.setLoggedInId(ContractApplicationTests.CONTRACTOR_ID);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void test_get_invoice_expecting_invoices_returned() throws Exception {
        List<InvoiceDTO> invoices = objectMapper.readValue(this.mockMvc.perform(MockMvcRequestBuilders.get("/contract/100/invoice"))
                .andReturn().getResponse().getContentAsString(), new TypeReference<List<InvoiceDTO>>() {
        });
        assertEquals(invoices.size(), 2);
    }

    @Test
    public void test_get_invoice_expecting_contract_not_found() throws Exception {
        ApplicationErrorDTO response = objectMapper.readValue(this.mockMvc.perform(MockMvcRequestBuilders.get("/contract/9999/invoice"))
                .andReturn().getResponse().getContentAsString(), new TypeReference<ApplicationErrorDTO>() {
        });

        assertEquals(response.code, "NOT_FOUND");
        assertEquals(response.message, "Contract: 9999 was not found");
    }

    @Test
    public void test_get_invoice_with_user_not_on_contract_expecting_not_found() throws Exception {
        demoUserRequestProvider.setLoggedInId(ContractApplicationTests.CUSTOMER_ID_2);
        ApplicationErrorDTO response = objectMapper.readValue(this.mockMvc.perform(MockMvcRequestBuilders.get("/contract/100/invoice"))
                .andReturn().getResponse().getContentAsString(), new TypeReference<ApplicationErrorDTO>() {
        });

        assertEquals(response.code, "NOT_FOUND");
        assertEquals(response.message, "Contract: 100 was not found");
    }

    @Test
    public void test_add_invoice_expecting_invoice_added() throws Exception {
        AddInvoiceDTO add = new AddInvoiceDTO();
        add.description = "Fix Gutters";
        add.cost = new BigDecimal("100.00");
        InvoiceDTO response = objectMapper.readValue(mockMvc.perform(MockMvcRequestBuilders.post("/contract/100/invoice")
                .content(objectMapper.writeValueAsString(add))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString(), InvoiceDTO.class);

        assertEquals("Fix Gutters", response.description);
        assertTrue(add.cost.compareTo(response.cost) == 0);
        assertEquals("SUBMITTED", response.status);
    }

    @Test
    public void test_add_invoice_contract_not_found() throws Exception {
        demoUserRequestProvider.setLoggedInId(ContractApplicationTests.CUSTOMER_ID_2);

        AddInvoiceDTO add = new AddInvoiceDTO();
        add.description = "Fix Gutters";
        add.cost = new BigDecimal("100.00");
        ApplicationErrorDTO response = objectMapper.readValue(mockMvc.perform(MockMvcRequestBuilders.post("/contract/100/invoice")
                .content(objectMapper.writeValueAsString(add))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString(), ApplicationErrorDTO.class);

        assertEquals(response.code, "NOT_FOUND");
        assertEquals(response.message, "Contract: 100 was not found");
    }

    @Test
    public void test_add_invoice_contract_budget_exceeded() throws Exception {
        AddInvoiceDTO add = new AddInvoiceDTO();
        add.description = "Fix Gutters";
        add.cost = new BigDecimal("10000.00");
        ApplicationErrorDTO response = objectMapper.readValue(mockMvc.perform(MockMvcRequestBuilders.post("/contract/100/invoice")
                .content(objectMapper.writeValueAsString(add))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString(), ApplicationErrorDTO.class);

        assertEquals("CONTRACT_MAX", response.code);
        assertEquals("Contract max value of 1000.00 has been exceeded.", response.message);
    }

    @Test
    public void test_update_status_expecting_status_updated() throws Exception {
        UpdateInvoiceDTO update = new UpdateInvoiceDTO();
        update.status = "VOID";
        update.description = "(Removed)";
        InvoiceDTO response = objectMapper.readValue(mockMvc.perform(MockMvcRequestBuilders.put("/contract/100/invoice/200")
                .content(objectMapper.writeValueAsString(update))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString(), InvoiceDTO.class);
        assertEquals(update.status, response.status);
        assertEquals(update.description, response.description);
    }

    @Test
    public void test_update_status_expecting_not_found() throws Exception {
        UpdateInvoiceDTO update = new UpdateInvoiceDTO();
        update.status = "VOID";
        update.description = "(Removed)";
        ApplicationErrorDTO response = objectMapper.readValue(mockMvc.perform(MockMvcRequestBuilders.put("/contract/100/invoice/10000")
                .content(objectMapper.writeValueAsString(update))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString(), ApplicationErrorDTO.class);
        assertEquals("NOT_FOUND", response.code);
        assertEquals("Invoice: 10000 was not found", response.message);

    }
}
