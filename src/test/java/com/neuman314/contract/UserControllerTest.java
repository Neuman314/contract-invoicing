package com.neuman314.contract;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neuman314.contract.dto.UserDTO;
import com.neuman314.contract.request.DemoUserRequestProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class UserControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DemoUserRequestProvider demoUserRequestProvider;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void test_get_users_expecting_3_users() throws Exception {
        List<UserDTO> users = objectMapper.readValue(this.mockMvc.perform(MockMvcRequestBuilders.get("/user"))
                .andReturn().getResponse().getContentAsString(), new TypeReference<List<UserDTO>>() {
        });
        assertEquals(users.size(), 3, "There were not exactly 3 users");
    }

    @Test
    public void test_loginas_expecting_user_3_loggedin() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/loginas")
                .content("{ \"id\": 3 }")
                .contentType(MediaType.APPLICATION_JSON));
        assertEquals(demoUserRequestProvider.getLoggedInId(), 3, "User 3 was not logged in.");
    }
}
