package com.neuman314.contract;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.neuman314.contract.dto.AddContractDTO;
import com.neuman314.contract.dto.ApplicationErrorDTO;
import com.neuman314.contract.dto.ContractDTO;
import com.neuman314.contract.request.DemoUserRequestProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class ContractControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DemoUserRequestProvider demoUserRequestProvider;

    @BeforeEach
    public void setup() throws Exception {
        demoUserRequestProvider.setLoggedInId(ContractApplicationTests.CONTRACTOR_ID);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void test_get_contracts_expecting_vendor_role() throws Exception {
        List<ContractDTO> contracts = objectMapper.readValue(this.mockMvc.perform(MockMvcRequestBuilders.get("/contract"))
                .andReturn().getResponse().getContentAsString(), new TypeReference<List<ContractDTO>>() {
        });

        ContractDTO contract = contracts.get(0);
        assertEquals(contract.role, "VENDOR", "Contractor role was not correct");
        assertEquals(contract.contractorUserId, 1L, "Contractor user id was not correct");
        assertEquals(contract.customerUserId, 2L, "Contractor customer id was not correct");
        assertEquals(contract.description, "New Roof Installation", "Contractor description was not correct");
        assertTrue(contract.maxValue.compareTo(new BigDecimal(1000)) == 0, "Contractor max value was not correct");
        assertEquals(contract.status, "APPROVED", "Contractor status was not correct");
    }

    @Test
    public void test_get_contracts_expecting_client_role() throws Exception {
        demoUserRequestProvider.setLoggedInId(ContractApplicationTests.CUSTOMER_ID);
        List<ContractDTO> contracts = objectMapper.readValue(this.mockMvc.perform(MockMvcRequestBuilders.get("/contract"))
                .andReturn().getResponse().getContentAsString(), new TypeReference<List<ContractDTO>>() {
        });

        ContractDTO contract = contracts.get(0);
        assertEquals(contract.role, "CLIENT", "Contractor role was not correct");
        assertEquals(contract.contractorUserId, 1L, "Contractor user id was not correct");
        assertEquals(contract.customerUserId, 2L, "Contractor customer id was not correct");
        assertEquals(contract.description, "New Roof Installation", "Contractor description was not correct");
        assertTrue(contract.maxValue.compareTo(new BigDecimal(1000)) == 0, "Contractor max value was not correct");
        assertEquals(contract.status, "APPROVED", "Contractor status was not correct");
    }

    @Test
    public void test_get_contracts_expecting_empty() throws Exception {
        demoUserRequestProvider.setLoggedInId(ContractApplicationTests.CUSTOMER_ID_2);
        List<ContractDTO> contracts = objectMapper.readValue(this.mockMvc.perform(MockMvcRequestBuilders.get("/contract"))
                .andReturn().getResponse().getContentAsString(), new TypeReference<List<ContractDTO>>() {
        });

        assertEquals(contracts.size(), 0, "Contracts was not empty");
    }

    @Test
    public void test_add_contract_expecting_contract_added() throws Exception {
        AddContractDTO contractDTO = new AddContractDTO();
        contractDTO.description = "test";
        contractDTO.maxValue = new BigDecimal(100);
        contractDTO.deadline = new Timestamp(System.currentTimeMillis() + 10000);
        contractDTO.customerId = 2L;

        ContractDTO response = objectMapper.readValue(mockMvc.perform(MockMvcRequestBuilders.post("/contract")
                .content(objectMapper.writeValueAsString(contractDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString(), ContractDTO.class);

        assertEquals(contractDTO.description, response.description);
        assertEquals(contractDTO.deadline, response.deadline);
        assertEquals(contractDTO.customerId, response.customerUserId);
        assertEquals(1L, response.contractorUserId);
        assertTrue(contractDTO.maxValue.compareTo(response.maxValue) == 0);
    }

    @Test
    public void test_add_contract_as_client_expecting_error() throws Exception {
        demoUserRequestProvider.setLoggedInId(ContractApplicationTests.CUSTOMER_ID);

        AddContractDTO contractDTO = new AddContractDTO();
        contractDTO.description = "test";
        contractDTO.maxValue = new BigDecimal(100);
        contractDTO.deadline = new Timestamp(System.currentTimeMillis() + 10000);
        contractDTO.customerId = 2L;

        ApplicationErrorDTO response = objectMapper.readValue(mockMvc.perform(MockMvcRequestBuilders.post("/contract")
                .content(objectMapper.writeValueAsString(contractDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString(), ApplicationErrorDTO.class);

        assertEquals(response.code, "INVALID_ROLE");
        assertEquals(response.message, "User must be a contractor in order to perform this action");
    }


}
